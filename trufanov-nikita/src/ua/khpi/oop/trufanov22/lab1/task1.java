package ua.khpi.oop.trufanov22.lab1;
public class task1 {
	public static void main(String[] args) {
		int zalik = 0xC9A078;
		long phonenumber = 380992505590L;	
		int last2 = 0b111011;
		int last4 = 012667;
		int mod = (22-1)%26+1;
		int i = mod+64;
		char letter = (char) i;
		long rest = 0;
        int count = 0;
        int odd = 0;
        int even = 0;
        char ZERO = '0';
		System.out.println(letter);
        while (phonenumber != 0) {
            rest = phonenumber % 10;
            phonenumber = phonenumber / 10;
            if (rest % 2 == 0)
                odd++;
            else{
            	even++;
            }
        }
        int ones = Integer.bitCount(last2);
        count = (int) Integer.toBinaryString(last2).chars().filter(item->item == ZERO).count();
        System.out.printf("odd=%d; even=%d number of ones = %d number of zeros =%d\n", odd, even, ones, count);
	}
}