package ua.khpi.oop.trufanov22.lab2;
import java.util.Random;
public class lab2 {
	public static void main(String[] args) {
		Random random = new Random();
		for (int i = 1; i < 11; i++) {
			int num = random.nextInt();
			System.out.printf("number[%d] = %d ", i, num);
			int j = sum(num);
			if (j < 0) {
				System.out.printf("sum[%d] = %d\n", i, sum(num)*-1);
			}
			else
			System.out.printf("sum[%d] = %d\n", i, sum(num));
		}
	}
	static int sum(int n) {
		int sum = 0;
        while (n != 0) {
            sum = sum + (n % 10);
            n /= 10;
        }
        return sum;
    }
}