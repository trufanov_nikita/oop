package ua.khpi.oop.trufanov22.lab5;

import java.util.Scanner;

//class conteiner {
//	String[] data  = new String[4];
//	int i = 0;
//	Mycontainer(int size, StringBuffer A){
//		data[1] = size;
//		data[2+1] = A;
//	}
//}

public class lab5 {
	static StringBuffer noDebug(StringBuffer A, int size) {
		boolean isVowel = true;

		for (int i = 0; i < A.length(); i++) {
			isVowel = true;
			int test = A.charAt(i);
			if (test == ' ') {
				continue;
			}
			for (int vowel : vowels) {
				if (test == vowel) {
					isVowel = false;
					break;
				}
			}
			if (isVowel) {
				int letters = 0;
				for (int j = i; j < A.length() && A.charAt(j) != ' '; j++) {
					letters++;
				}
				if (letters == size) {
					A.delete(i, i + letters);
				} else {
					i += letters;
				}
			} else {
				for (int k = i; k < A.length() && A.charAt(k) != ' '; k++, i++) {
				}
			}
		}
		for (int i = 0; i < A.length() - 1; i++) {
			if (A.charAt(i) == ' ' && A.charAt(i + 1) == ' ') {
				A.deleteCharAt(i);
				i--;
			}
		}
		if (A.charAt(0) == ' ') {
			A.deleteCharAt(0);
		}

		return (A);
	}

	static final int[] vowels = { 'a', 'A', 'i', 'I', 'u', 'U', 'y', 'Y', 'e', 'E', 'o', 'O' };

	static StringBuffer Debug(StringBuffer A, int size) {
		boolean isVowel = true;

		System.out.println("Our string we want to change");
		System.out.println(A);
		System.out.println("-------------------");
		for (int i = 0; i < A.length(); i++) {
			isVowel = true;
			int test = A.charAt(i);
			if (test == ' ') {
				continue;
			}
			for (int vowel : vowels) {
				if (test == vowel) {
					isVowel = false;
					break;
				}
			}
			if (isVowel) {
				int letters = 0;
				for (int j = i; j < A.length() && A.charAt(j) != ' '; j++) {
					letters++;
				}
				if (letters == size) {
					A.delete(i, i + letters);

					System.out.println("After deleting word");
					System.out.println(A);
					System.out.println("-------------------");
				} else {
					i += letters;
				}
			} else {
				for (int k = i; k < A.length() && A.charAt(k) != ' '; k++, i++) {
				}
			}
		}
		for (int i = 0; i < A.length() - 1; i++) {
			if (A.charAt(i) == ' ' && A.charAt(i + 1) == ' ') {
				A.deleteCharAt(i);
				i--;
			}
		}
		if (A.charAt(0) == ' ') {
			A.deleteCharAt(0);
		}
		System.out.println("Now without spaces");
		System.out.println(A);
		System.out.println("-------------------");
		return (A);
	}

	static void help() {
		System.out.println(
				"Instructions:\n1 write string in english\n2 for choose variants write number in console\n3 in size of word write normal numbers");
	}

	static StringBuffer insertData() {
		Scanner scan = new Scanner(System.in);
		String s = "";
		s = scan.nextLine();
		StringBuffer A = new StringBuffer(s);
		return (A);
	}

	static void showData(StringBuffer txt, int num) {
		System.out.println(txt);
		System.out.println(num);
	}

	static void printResult(StringBuffer A, StringBuffer B) {
		System.out.println("Result of changing your string is:");
		System.out.println(A);
		System.out.println("Your string before changing is:");
		System.out.println(B);
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Boolean isDebug = false;
		for (int i = 0; i < args.length; i++) {
			String str = args[i];
			if (str.equals("-h") || str.equals("-help")) {
				help();
			}
			if (str.equals("-d") || str.equals("-debug")) {
				isDebug = true;
			}
		}
		int x = 0;
		String s = "";
		int size = 0;

		StringBuffer A = new StringBuffer(s);
		StringBuffer B = new StringBuffer(s);
		while (!"6".equals(s)) {
			System.out.println("------------------------\n1. Insert Data");
			System.out.println("2. Show data");
			System.out.println("3. Start calculating");
			System.out.println("4. show result");
			System.out.println("5. show instructions");
			System.out.println("6. End\n -----------------------");
			s = scan.next();

			try {
				x = Integer.parseInt(s);
			} catch (NumberFormatException e) {
				System.out.println("False enter");
			}

			switch (x) {
			case 1:
				int number = scan.nextInt();
				size = number;
				A.append(insertData());
				B.append(A);
				break;
			case 2:
				showData(A, size);
				break;
			case 3:
				if (isDebug) {
					Debug(A, size);
				} else {
					noDebug(A, size);
				}
				break;
			case 4:
				printResult(noDebug(A, size), B);
				break;
			case 5:
				help();
				break;
			case 6:
				break;
			}
		}
		System.out.println("End");

	}
}