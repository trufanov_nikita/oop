package ua.khpi.oop.trufanov22.lab3;

public class lab3 {
	static void show(StringBuffer txt) {
		System.out.println("Length " + txt.length());
		System.out.println("Capacity " + txt.capacity());
	}

	static final int[] vowels = { 97, 101, 105, 111, 117, 121 };

	public static void main(String[] args) {
		String txt = "otext textr   texttext oop andrey             text text dlast";
		String str = txt.toLowerCase();
		StringBuffer A = new StringBuffer(str);
		StringBuffer B = new StringBuffer(str);
		int size = 4;
		boolean isVowel = true;

		for (int i = 0; i < A.length(); i++) {
			isVowel = true;
			int test = A.charAt(i);
			if (test == ' ') {
				continue;
			}
			for (int vowel : vowels) {
				if (test == vowel) {
					isVowel = false;
					break;
				}
			}
			if (isVowel) {
				int letters = 0;
				for (int j = i; j < A.length() && A.charAt(j) != ' '; j++) {
					letters++;
				}
				if (letters == 4) {
					A.delete(i, i + letters);
				}
				else {
					i+=letters;
				}
			} else {
				for (int k = i; k < A.length() && A.charAt(k) != ' '; k++, i++) {
				}
			}
		}
		for (int i = 0; i < A.length() - 1; i++) {
			if (A.charAt(i) == ' ' && A.charAt(i + 1) == ' ') {
				A.deleteCharAt(i);
				i--;
			}
		}
		if(A.charAt(0) == ' ') {
			A.deleteCharAt(0);
		}
		System.out.println(B);
		System.out.println(A);

		show(A);

	}
}
